"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
main.py:
    Entry point to run completion for ML model
"""
from flask import Flask
from flask import send_file
from src.argconfig import get_args

from src.model_saver import compile_model
from src.logger import get_logger

app = Flask(__name__)

logger = get_logger()
args = get_args()

# Define a function to save the compiled model locally and provide a download link
def save_model_locally():
    """
    Starts Flask UI to be able to serve the compiled ".so" TVM model

    Args:
        None

    Returns:
        None
    """
    logger.info("The model is saved locally... Please copy the below address and paste in a browser to download the compiled model")
    logger.info("MODEL DOWNLOAD LINK: http://127.0.0.1:8060/download")
    app.run(port=8060)

# Define a route for downloading files using Flask
@app.route('/download')
def downloadFile():
    """
    Endpoint for downloading a file as an attachment.

    This route allows users to download a TVM Compiled model file as an attachment. 
    The path to the file is provided through the 'output_file' parameter in the 
    request's query parameters. The file will be returned with appropriate headers 
    to indicate that it should be treated as an attachment for download.

    Returns:
        Response: A Flask Response object representing the file attachment.
                  The file will be streamed to the client for download.

    Example:
        If the following URL is requested:
        http://127.0.0.1/download

        The 'model.zip' located at the server will be
        streamed to the client as a downloadable attachment.
    """
    path = args["output_file"]
    return send_file(path, as_attachment=True)

if __name__ == "__main__":
    compile_model(args)
    
    if args["platform"] == "local":
        save_model_locally()