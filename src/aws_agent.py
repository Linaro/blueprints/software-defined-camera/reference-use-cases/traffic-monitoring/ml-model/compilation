"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
aws_agent.py:
    Applying the configuration for uploading files to AWS bucket
"""

import boto3
from tqdm import tqdm
import os
import json

from src.logger import get_logger

logger = get_logger()

# Define a class for interacting with AWS services
class AwsAgent:
    def __init__(self):
        """
        Constructor of AwsAgent class.

        This function loads the cloud account configuration & open the authenticated
        connection with AWS cloud service for S3 storage service. The created
        S3 instance will be used for download the ONNX model and stores the compiled
        TVM model into the S3 storage so it can be served to the inference container.

        Parameters:
        None

        Returns:
        None
        """
        # Load AWS configuration from a JSON file
        aws_data = self.load_aws_config("config/aws_config.json")
        if aws_data is None:
            logger.warning("AWS config file is not available or invalid.")
            raise ValueError("AWS config file is not available or invalid.")
        
        # Create an S3 client using the loaded credentials and region
        self.s3 = boto3.client('s3', 
                        aws_access_key_id=aws_data["access_key"], 
                        aws_secret_access_key=aws_data["secret_key"], 
                        region_name=aws_data["region"]
                        )
        
        # Check if the connection to the S3 bucket is successful
        try:
            self.s3.head_bucket(Bucket=aws_data["bucket"])
        except Exception as e:
            logger.warning(f"Failed to connect to AWS S3: {e}")
            raise ConnectionError(f"Failed to connect to AWS S3: {e}")

    # Load AWS configuration data from a JSON file   
    def load_aws_config(self, config_path):
        """
        Load the AWS cloud account configuration.
        
        Parameters:
        config_path: Path of json configuration file to load the cloud credentials.

        Returns:
        None
        """
        if not os.path.exists(config_path):
            return None
        with open(config_path) as json_data_file:
            aws_data = json.load(json_data_file)
        return aws_data
    
    # Download a file from S3
    def download_from_s3(self, bucket, key, filename):
        """
        Downloads the Fused ONNX model from S3 bucket and stores to the path
        specified in the parameters.
        
        Parameters:
        bucket: Configured S3 bucket name.
        key: Folder path of S3 for ONNX model.
        filename: Name of ONNX model file to be downloaded.

        Returns:
        None
        """
        try:
            # Fetch metadata about the S3 object
            meta_data = self.s3.head_object(Bucket=bucket, Key=key)
        except Exception as e:
            logger.warning(f"Failed to fetch S3 object metadata: {e}")
            raise RuntimeError(f"Failed to fetch S3 object metadata: {e}")
        
        # Get the total file size from metadata
        total_length = int(meta_data.get('ContentLength', 0))
        if total_length <= 0:
            logger.warning("Invalid ContentLength received from S3.")
            raise ValueError("Invalid ContentLength received from S3.")

        # Display download progress
        logger.info(f"Downloading the model : {key} From bucket : {bucket}, File size : {round(total_length/1024/1024)} MB")
        
        try:
            # Use tqdm for visual progress bar during download
            with tqdm(total=total_length,  desc=f'source: s3://{bucket}/{key}', 
                bar_format="{percentage:.1f}%|{bar:25} | {rate_fmt} | {desc}",  unit='B', unit_scale=True, unit_divisor=1024) as pbar:
                self.s3.download_file(bucket, key, Filename=filename, Callback=pbar.update)
        except Exception as e:
            logger.warning(f"Error during S3 file download: {e}")
            raise RuntimeError(f"Error during S3 file download: {e}")
    
    # Upload a file to S3
    def upload_to_s3(self, bucket, key, filename):
        """
        Uploads the Compiled TVM model to S3 bucket and stores to the path
        specified in the parameters.
        
        Parameters:
        bucket: Configured S3 bucket name.
        key: Folder path of S3 for TVM model to be saved.
        filename: Name of ONNX model file to be uploaded.

        Returns:
        None
        """
        try:
            # Get the file size for uploading
            file_size = os.stat(filename).st_size
            if file_size <= 0:
                logger.warning("Invalid file size for upload.")
                raise ValueError("Invalid file size for upload.")
            # Display upload progress
            logger.info(f"Uploading the model : {filename} To bucket : {bucket}, File size : {round(file_size/1024/1024, 3)} MB")
            # Use tqdm for visual progress bar during upload
            with tqdm(total=file_size, bar_format="{percentage:.1f}%|{bar:50} | {rate_fmt} | {desc}" ,unit="B", unit_scale=True, desc=filename) as pbar:
                self.s3.upload_file(Filename=filename,Bucket=bucket,Key=key,
                            Callback=lambda bytes_transferred: pbar.update(bytes_transferred),
                            )
        except Exception as e:
            logger.warning(f"Error during S3 file upload: {e}")
            raise RuntimeError(f"Error during S3 file upload: {e}")
