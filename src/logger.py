import logging
import logging.config

def get_logger(file_path="config/logger.conf"):
    logging.config.fileConfig(file_path)
    # creating logger
    logger = logging.getLogger('ModelCompilation')
    return logger